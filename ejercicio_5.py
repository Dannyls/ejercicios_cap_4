#___Ejercicio 5: ¿Qué mostrará en pantalla el siguiente programa Python?___
#___Author___ = "Danny Lima"
# ___Email___ = "danny.lima@unl.edu.ec

def fred():
    print("Zap")
def jane():
    print("ABC")

jane()
fred()
jane()

#A continuación estan las alternativas.
#Elija la correcta.
# a) Zap ABC jane fred jane
# b) Zap ABC Zap
# c) ABC Zap jane
# d) ABC Zap ABC
# e) Zap Zap Zap
print("El literal correcto es: ABC  Zap  ABC")
print("GRACIAS")
