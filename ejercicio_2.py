# ___Ejercicio 1:Desplaza la última línea del programa anterior hacia arriba,
# de modo que la llamada a la función aparezca antes que las definiciones.
# Ejecuta el programa y observa qué mensaje de error obtienes.
# ___Author___ = "Danny Lima"
# ___Email___ = "danny.lima@unl.edu.ec

repite_estribillo()

def muestra_estribillo():
    print("Soy un estudiante, que privilegio.")
    print("Estudio todo el dia y duermo en la noche.")


def repite_estribillo():
    muestra_estribillo()
    muestra_estribillo()

#Se presenta un mensaje de error que presenta el programa es NameError: debido a que la función :'repite_estribillo'
# no está definido.
