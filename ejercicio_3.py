#___Ejercicio 3: Desplaza la llamada de la función de nuevo hacia el final,
#y coloca la definición de muestra_estribillo después de la definición de
#repite_estribillo___
# ___Author___ = "Danny Lima"
# ___Email___ = "danny.lima@unl.edu.ec

def repite_estribillo():
    muestra_estribillo()
    muestra_estribillo()

def muestra_estribillo():
    print("Soy un estudiante, que privilegio.")
    print("Estudio todo el dia y duermo en la noche.")

repite_estribillo()

# Se imprime por pantalla:
# Soy un estudiante, que privilegio.
# Estudio todo el el dia y duermo en la noche.
# Soy un estudiante, que privilegio.
# Estudio todo el dia y duermo en la noche.

