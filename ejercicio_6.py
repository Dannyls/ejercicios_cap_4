#___Ejercicio 6: Programa  para horas extras, y crear una función llamada
# calculo_salario que reciba dos parámetros (horas y tarifa).
# ___Author___ = "Danny Lima"
# ___Email___ = "danny.lima@unl.edu.ec

try:
    horas = int(input("Ingrese las horas:"))
    tarifa = float(input("Ingrese la tarifa por hora:"))


    def calculo_salario(horas,tarifa):
        if horas > 40:
            hora_extra = horas - 40
            print("Las horas extras son:", hora_extra)
            tarifa_extra = (hora_extra * 1.5) * tarifa
            print("La tarifa extra es:", tarifa_extra)
            salario = (40 * tarifa) + tarifa_extra
            print("El salario es:", salario)
            print("Gracias por consultar aquí!")
        else:
            salario = horas * tarifa
            print("El salario es:", salario)
            print("Gracias por consultar aquí")


    calculo_salario(horas,tarifa)
except:
    print("Error, por favor ingrese un número")
